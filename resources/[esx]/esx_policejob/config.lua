Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = false
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = true -- turn this on if you want custom peds
Config.EnableLicenses             = false -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.PoliceStations = {

	LSPD = {

		Blip = {
			Coords  = vector3(425.1, -979.5, 30.7),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(452.6, -992.8, 28.6)
		},

		Armories = {
			vector3(453.55, -980.1, 30.01)
		},

		Vehicles = {
			{
				Spawner = vector3(454.6, -1017.4, 28.4),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(438.4, -1018.3, 27.7), heading = 90.0, radius = 6.0 },
					{ coords = vector3(441.0, -1024.2, 28.3), heading = 90.0, radius = 6.0 },
					{ coords = vector3(453.5, -1022.2, 28.0), heading = 90.0, radius = 6.0 },
					{ coords = vector3(450.9, -1016.5, 28.1), heading = 90.0, radius = 6.0 }
				}
			},

			{
				Spawner = vector3(473.3, -1018.8, 28.0),
				InsideShop = vector3(228.5, -993.5, -99.0),
				SpawnPoints = {
					{ coords = vector3(475.9, -1021.6, 28.0), heading = 276.1, radius = 6.0 },
					{ coords = vector3(484.1, -1023.1, 27.5), heading = 302.5, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(461.1, -981.5, 43.6),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(449.5, -981.2, 43.6), heading = 92.6, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(465.42, -1009.27, 35.93)
		}

	},

	SSPD = {

		Blip = {
			Coords  = vector3(1855.23, 3692.56, 39.04),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(1858.83, 3695.6, 32.23)
		},

		Armories = {
			vector3(1861.25, 3692.05, 34.00)
		},

		Vehicles = {
			{
				Spawner = vector3(1865.42, 3689.53, 34.27),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(1868.65, 3695.68, 33.19), heading = 210.58, radius = 6.0 },
					{ coords = vector3(1866.1, 3693.91, 33.5), heading = 210.17, radius = 6.0 },
					{ coords = vector3(1861.57, 3707.75, 32.94), heading = 211.5, radius = 6.0 },
					{ coords = vector3(1859.18, 3706.32, 33.08), heading = 209.91, radius = 6.0 }
				}
			}
		},

		BossActions = {
			vector3(1857.24, 3700.01, 34.22)
		}

	},

	PBPD = {

		Blip = {
			Coords  = vector3(-444.2, 6007.77, 40.53),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 29
		},

		Cloakrooms = {
			vector3(-433.11, 6001.71, 30.72)
		},

		Armories = {
			vector3(-440.44, 5992.46, 30.56)
		},

		Vehicles = {
			{
				Spawner = vector3(-458.95, 6016.64, 31.49),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(-462.23, 6009.28, 30.95), heading = 266.08, radius = 6.0 },
					{ coords = vector3(-458.05, 6005.28, 30.95), heading = 265.28, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(-451.09, 5985.05, 31.36),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(-480.35, 5984.08, 32.18), heading = 315.73, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(-437.55, 5998.23, 30.72)
		}

	}

}

Config.AuthorizedWeapons = {
	recruit = {
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 }
	},

	officer = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	corporal = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},
	
	sergeant = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	lieutenant = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	boss = {
		{ weapon = 'WEAPON_COMBATPISTOL', components = { 0, 9999999, 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_CARBINERIFLE', components = { 0, 0, 9999999, 0, 0, nil }, price = 0 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 0, 9999999, nil }, price = 0 },
		{ weapon = 'WEAPON_FIREEXTINGUISHER', price = 0 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_FLARE', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 0 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	}	
}

Config.AuthorizedVehicles = {
	Shared = {
		{ model = 'tahoe13leo', label = '13 Tahoe', price = 1 },
		{ model = 'ram', label = '16 Ram', price = 1 },
		{ model = '17tahoe', label = '17 Tahoe', price = 1 },
		{ model = 'bcso2', label = '18 Charger', price = 1 },
		{ model = '19durango', label = '19 Durango', price = 1 },
		{ model = 'patrolcap3', label = 'Caprice', price = 1 },
		{ model = 'polchgr', label = 'Charger', price = 1 },
		{ model = 'chargerk9', label = 'K9 Charger', price = 1 },
		{ model = 'ChevySS', label = 'Chevy SS', price = 1 },
		{ model = '11vic', label = '11 Vic', price = 1 },
		{ model = '13explorer', label = '13 Explorer', price = 1 },
		{ model = '14charger', label = '14 Charger', price = 1 },
		{ model = '16taurus', label = '16 Taurus', price = 1 },
		{ model = '18charger', label = '2018 Charger', price = 1 },
		{ model = 'exp16', label = '16 Explorer', price = 1 },
		{ model = 'silv', label = 'Silv', price = 1 },
		{ model = '16explorer', label = '2016 Explorer', price = 1 },
		
	},

	officer = {

	},

	corporal = {

	},

	sergeant = {

	},

	lieutenant = {
		{model = 'camaroRB', label = 'Camaro', price = 1 },
		{ model = 'ZL1', label = 'ZL 1', price = 1 },

	},

	boss = {
		{ model = 'hellcat', label = 'Hellcat', price = 1 },
		{ model = 'camaroRB', label = 'Camaro', price = 1 },
		{ model = 'VettePD', label = 'Corvette', price = 1 },
		{ model = 'ZL1', label = 'ZL 1', price = 1 },


	},


}

Config.AuthorizedHelicopters = {
	boss = {
		{ model = 'eheli', label = 'AS350', livery = 0, price = 1 }
	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	recruit_wear = {}

}