TokoVoipConfig = {
	channels = {
		{name = "Call PD Radio", subscribers = {}},
		{name = "EMS/Fire Radio", subscribers = {}},
		{name = "PD Radio", subscribers = {}},
		{name = "County Radio", subscribers = {}},
		{name = "State Radio", subscribers = {}},
		{name = "LEO/EMS Shared Radio", subscribers = {}},
		{name = "PD/SO/State Shared Radio", subscribers = {}}
	}
};
