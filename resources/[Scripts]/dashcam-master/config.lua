DashcamConfig = {}

DashcamConfig.useMPH = true -- False will turn it to KMH

DashcamConfig.RestrictVehicles = true
DashcamConfig.RestrictionType = "class" -- custom / class

DashcamConfig.AllowedVehicles = {
    "14charger",
    "18charger",
    "16exp",
    "13explorer",
}