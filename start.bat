@ECHO OFF
:choice
set /P c=Would you like to delete your server cache?[Y/N]?
if /I "%c%" EQU "Y" goto :somewhere
if /I "%c%" EQU "N" goto :somewhere_else
goto :choice
:somewhere
echo "Deleting server cache"
rd /s /q "C:\ngrp-server-1\cache\"
echo -
echo 
echo -
start C:\ngrp-server-1\run.cmd +exec server.cfg
exit
:somewhere_else
echo
@echo off
echo -
echo 
echo -
start C:\ngrp-server-1\run.cmd +exec server.cfg
exit